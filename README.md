# Solidarites International x Latitudes

## Contexte
* Solidarités International est une ONG internationale d’origine française fondée en 1980, ayant pour but d'apporter l'aide humanitaire nécessaire aux populations en crise. L’association à but non lucratif a pour but de répondre aux besoins vitaux des populations confrontées à une crise humanitaire résultant de conflit armé ou de catastrophe naturelle mais aussi d’accompagner le retour des victimes vers l’autonomie et le développement, notamment dans les domaines de l’eau, de l’hygiène, de l’assainissement et de la sécurité alimentaire.
* En 2018 elle est intervenue dans 15 pays, Cameroun, Mali, République Centrafricaine, RD Congo, Nigeria, Soudan du Sud, Tchad, Afghanistan, Bangladesh, Myanmar, Pakistan, Haïti, Liban, Syrie et Irak
* https://www.solidarites.org/fr/

## À propos du projet
* Une des missions récurrentes en Afrique et au Moyen Orient est d’installer rapidement des kits de pompage sur les lieux en tension, qui puissent ensuite être durables, et utilisable par les locaux. Devant la hausse du prix du pétrole, sa raréfaction et son impact carbone, il apparaît pertinent d’investir dans ces systèmes de pompage à énergie renouvelable, et dans le cas des pays d’intervention de l’ONG, des systèmes solaires.
* Solidarités International souhaite que nous concevions un outil d’aide au choix de système, donc un outil de dimensionnement de pompe, de kit solaire et un guide d’installation
* Le cadre dans lequel cela s'inscrit : Tech for Good Explorers

## Perspectives d'évolution
* Développés un outil avec une technologie plus performante (Programme, application ...)
* Numérisation des courbes de performances des pompes des constructeurs
* Scrapping des données d'ensoleillement de la NASA en fonction des coordonnées GPS

## Contact
* Bansaga SAGA bsaga@solidarite.org
* Natalie PERRONE et Baptiste ALBERT, Coachs chez Ekimetrics natalie.perrone@equimetrics.com baptiste.albert@ekimetrics.com
* Matteo, Hugo et Paul matteo.motterlini@student.ecp.fr hugo.witt@student.ecp.fr paul.martin@student.ecp.fr